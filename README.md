# ezMarker
ezMarker is a web application which extends a [Brightspace learning enviroment](https://www.d2l.com/products/learning-environment/) by enabling bulk upload of numeric student grades **and** feedback (current Brighspace systems can only bulk upload grades. Feedback has to be manually added.). The program uses the [Brightspace API](http://docs.valence.desire2learn.com/reference.html) to add this additional functionality.  A writeup with more background can be found [here](..)

There is a PHP and a Python version but as of version 2.0.0 only the PHP code is supported. The Python code is left as a reference for anyone doing something with the [Brightspace API](http://docs.valence.desire2learn.com/reference.html). Starting with version 2.0.0 the official version of the code can be found [here](https://bitbucket.org/harohodg/ezmarker/src/master/). Version 1.0.0 (found [here](https://github.com/Sjohnston3700/CP317_MLS_Project/)) was a student group project for [CP317](https://bohr.wlu.ca/cp317/) and is left as is.

If you just want to use the PHP code please continue [here](https://bitbucket.org/harohodg/ezmarker/src/php/README.md). If you're are interested in how the program works under the hood and/or want to play around with the code have a look at the documentation [here](https://bitbucket.org/harohodg/ezmarker/wiki/Home).

## Contributing
If you are interested in contributing to this project please send us an [email](mailto:harold.hodgins@mypurplecrayon.com).

## Versioning
We are trying to follow [semantic versioning](https://semver.org/) rules for versioning ezmarker. The current major release is v2 which is functional, feature complete, and 'safe' to use. It is of course offered "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. See [LICENSE.md](LICENSE.md) for more details.

## Authors
* *Version 0.0.0* - **Harold Hodgins** - [CP493 - Winter 2017](https://cocalc.com/share/9501f241-b52e-43f8-9034-7292e8ee54ce/final_report/MLS_API_Report.pdf?viewer=share)
* *Version 1.0.0* - **Harold Hodgins** and [contributors](https://github.com/Sjohnston3700/CP317_MLS_Project/contributors)
* *Version 2.0.0* - **Harold Hodgins**

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
* This Readme is based on the [Purple Booth ReadMe template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* A big thank you to everyone who contributed to v1 of this project


